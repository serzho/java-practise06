package com.practise;

import java.util.*;

public class CashMachine {

    protected Map<Item, Integer> processed;

    public CashMachine() {
        processed = new HashMap<>();
    }
    
    public void process(Item item) {
        if (contains(item))
            updateQuantity(item);
        else
            add(item);
    }

    public Check formCheck() {
        return Check.form(processed);
    }

    protected boolean contains(Item item) {
        return processed.containsKey(item);
    }

    protected void updateQuantity(Item item) {
        int quantity = processed.get(item);
        processed.put(item, quantity + 1);
    }

    protected void add(Item item) {
        processed.put(item, 1);
    }
}
