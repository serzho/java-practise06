package com.practise;

import java.util.*;

public class Check {
    protected static class CheckItem {
        private String name;
        private long price;
        private int quantity;
        private long total;

        public CheckItem(String name, long price, int quantity, long total) {
            this.name = name;
            this.price = price;
            this.quantity = quantity;
            this.total = total;
        }

        public boolean equals(Object other) {
            if (other == null) return false;
            if (other == this) return true;
            if (!other.getClass().equals(this.getClass())) return false;
            CheckItem t = (CheckItem) other;
            if (!t.name.equals(this.name)) return false;
            if (!(t.price == this.price)) return false;
            if (!(t.quantity == this.quantity)) return false;
            if (!(t.total == this.total)) return false;
            return true;
        }

        public String toString() {
            return name;
        }
    }

    protected List<CheckItem> checkItems;

    protected Check() {
        checkItems = new ArrayList();
    }

    protected void addItem(String name, long price, int quantity) {
        long total = price * quantity;
        checkItems.add(new CheckItem(name, price, quantity, total));
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!other.getClass().equals(this.getClass())) return false;
        Check t = (Check) other;
        if (!t.checkItems.equals(this.checkItems)) return false;
        return true;
    }

    public static Check form(Map<Item, Integer> items) {
        Check check = new Check();

        for (Item item : items.keySet()) {
            int quantity = items.get(item);
            check.addItem(item.getName(), item.getPrice(), quantity);
        }

        return check;
    }
}
