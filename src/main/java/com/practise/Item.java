package com.practise;

public class Item {
    protected String name;
    protected long price;

    public Item(String name, long price) {
        this.name = name;
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }

    public void setPrice(long price) {
        this.price = price;
    }

    public long getPrice() {
        return price;
    }

    public boolean equals(Object other) {
        if (other == null) return false;
        if (other == this) return true;
        if (!other.getClass().equals(this.getClass())) return false;
        Item t = (Item) other;
        if (!t.name.equals(this.name)) return false;
        if (!(t.price == this.price)) return false;
        return true;
    }

    public int hashCode() {
        int hash = 5;
        hash = 89 * hash + (this.name != null ? this.name.hashCode() : 0);
        hash = 89 * hash + (int) (this.price ^ (this.price >>> 32));
        return hash;
    }
}
