package com.practise;

import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class CashMachineTest {

    CashMachine cashMachine;
    Item car;

    @Before
    public void up() {
        cashMachine = new CashMachine();
        car = new Item("car", 2000);
    }

    @Test
    public void should_add_item_if_it_was_not_added_before() {
        cashMachine.process(car);

        assertThat(cashMachine.processed, hasKey(car));

        int quantity = cashMachine.processed.get(car);
        assertThat(quantity, is(1));
    }

    @Test
    public void should_update_quantity_if_item_was_added() {
        cashMachine.process(car);
        cashMachine.process(car);

        assertThat(cashMachine.processed, hasKey(car));

        int quantity = cashMachine.processed.get(car);
        assertThat(quantity, is(2));
    }

    @Test
    public void should_form_not_null_check() {
        assertThat(cashMachine.formCheck(), is(notNullValue()));
    }
}
