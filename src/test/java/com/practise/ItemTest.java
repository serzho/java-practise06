package com.practise;

import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class ItemTest {

    @Test
    public void should_have_name_and_price() {
        Item item = new Item("toy", 1000);

        assertThat(item.name, is("toy"));
        assertThat(item.price, is(1000L));
    }

    @Test
    public void shoud_equals() {
        Item toy = new Item("toy", 1000);
        Item sameToy = new Item("toy", 1000);

        assertThat(toy, is(sameToy));
    }
}
