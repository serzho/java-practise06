package com.practise;

import java.io.*;
import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;
import org.junit.*;

public class CheckTest {

    @Test
    public void should_generate_valid_check() {
        CashMachine cashMachine = new CashMachine();
        Item car = new Item("car", 1000);
        Item plane = new Item("plane", 2000);

        cashMachine.process(car);
        cashMachine.process(car);
        cashMachine.process(plane);

        Check check = cashMachine.formCheck();
        
        Check.CheckItem carInCheck = new Check.CheckItem(
                car.getName(), car.getPrice(), 2, 2000L);

        Check.CheckItem planeInCheck = new Check.CheckItem(
                plane.getName(), plane.getPrice(), 1, 2000L);

        assertThat(check.checkItems, hasItem(carInCheck));
        assertThat(check.checkItems, hasItem(planeInCheck));
    }
}
